Source: congress
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
 Michal Arbet <michal.arbet@ultimum.io>,
Build-Depends:
 debhelper-compat (= 10),
 dh-python,
 openstack-pkg-tools (>= 99~),
 po-debconf,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 alembic,
 antlr3,
 python3-antlr3,
 python3-aodhclient,
 python3-cinderclient,
 python3-coverage,
 python3-cryptography,
 python3-dateutil,
 python3-eventlet,
 python3-fixtures,
 python3-glanceclient,
 python3-hacking,
 python3-heatclient,
 python3-ironicclient,
 python3-jsonpath-rw,
 python3-jsonschema,
 python3-keystoneauth1,
 python3-keystoneclient,
 python3-keystonemiddleware,
 python3-mistralclient,
 python3-mock,
 python3-monascaclient,
 python3-mox3,
 python3-muranoclient,
 python3-netaddr,
 python3-neutronclient,
 python3-novaclient,
 python3-openstackdocstheme <!nodoc>,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.db,
 python3-oslo.log,
 python3-oslo.messaging,
 python3-oslo.middleware,
 python3-oslo.policy,
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.upgradecheck,
 python3-oslo.utils,
 python3-oslo.vmware,
 python3-oslotest,
 python3-paste,
 python3-pastedeploy,
 python3-psycopg2,
 python3-pulp,
 python3-requests,
 python3-requests-mock,
 python3-routes,
 python3-six,
 python3-sphinxcontrib.apidoc,
 python3-stestr,
 python3-swiftclient,
 python3-tackerclient,
 python3-tenacity,
 python3-testscenarios,
 python3-testtools,
 python3-webob,
 python3-yaml,
 sixer,
 subunit,
Standards-Version: 4.1.3
Vcs-Browser: https://salsa.debian.org/openstack-team/services/congress
Vcs-Git: https://salsa.debian.org/openstack-team/services/congress.git
Homepage: https://github.com/openstack/congress

Package: congress-common
Architecture: all
Depends:
 adduser,
 dbconfig-common,
 debconf,
 python3-congress (= ${binary:Version}),
 sqlite3,
 ${misc:Depends},
 ${python3:Depends},
Description: OpenStack Policy as a Service - common files
 Congress is an OpenStack project to provide policy as a service across any
 collection of cloud services in order to offer governance and compliance for
 dynamic infrastructures.
 .
 This package contains common files and configuration that are
 needed by all the daemon packages of Cinder.

Package: congress-server
Architecture: all
Depends:
 adduser,
 congress-common (= ${binary:Version}),
 debconf,
 q-text-as-data,
 ${misc:Depends},
 ${ostack-lsb-base},
 ${python3:Depends},
Description: OpenStack Policy as a Service - API server
 Congress is an OpenStack project to provide policy as a service across any
 collection of cloud services in order to offer governance and compliance for
 dynamic infrastructures.
 .
 This package contains the API server on which clients will connect.

Package: python3-congress
Section: python
Architecture: all
Depends:
 alembic,
 antlr3,
 python3-aodhclient,
 python3-cinderclient,
 python3-cryptography,
 python3-dateutil,
 python3-eventlet,
 python3-glanceclient,
 python3-heatclient,
 python3-ironicclient,
 python3-jsonpath-rw,
 python3-jsonschema,
 python3-keystoneauth1,
 python3-keystoneclient,
 python3-keystonemiddleware,
 python3-mistralclient,
 python3-monascaclient,
 python3-muranoclient,
 python3-netaddr,
 python3-neutronclient,
 python3-novaclient,
 python3-oslo.concurrency,
 python3-oslo.config,
 python3-oslo.context,
 python3-oslo.db,
 python3-oslo.log,
 python3-oslo.messaging,
 python3-oslo.middleware,
 python3-oslo.policy,
 python3-oslo.serialization,
 python3-oslo.service,
 python3-oslo.upgradecheck,
 python3-oslo.utils,
 python3-oslo.vmware,
 python3-paste,
 python3-pastedeploy,
 python3-pbr,
 python3-psycopg2,
 python3-pulp,
 python3-requests,
 python3-routes,
 python3-six,
 python3-swiftclient,
 python3-tackerclient,
 python3-tenacity,
 python3-webob,
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
Breaks:
 python-congress,
Replaces:
 python-congress,
Description: OpenStack Policy as a Service - Python libraries
 Congress is an OpenStack project to provide policy as a service across any
 collection of cloud services in order to offer governance and compliance for
 dynamic infrastructures.
 .
 This package contains the Python libraries that are part of Congress.
